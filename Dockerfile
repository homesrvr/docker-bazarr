### base stage ###
FROM python:3.10 as base

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
WORKDIR /app/bazarr
RUN \
  apt-get update && \
  apt-get install --no-install-recommends -y curl jq unzip build-essential libpq-dev && \
  BAZARR_VERSION=$(curl -s https://api.github.com/repos/morpheus65535/bazarr/releases/latest | jq -r '.tag_name') && \
  export BAZARR_VERSION && \
  curl -s -L -o /tmp/bazarr.zip "https://github.com/morpheus65535/bazarr/releases/download/${BAZARR_VERSION}/bazarr.zip" && \
  mkdir -p /app/bazarr && \
  unzip /tmp/bazarr.zip -d /app/bazarr && \
  pip install --no-cache-dir --upgrade pip && \
  pip install --no-cache-dir -r requirements.txt

#### stage 2 ####
FROM python:3.10-slim-buster
# install numpy library
RUN \
  apt-get update && \
  apt-get install --no-install-recommends -y curl ffmpeg unzip && \
  pip install --no-cache-dir --upgrade pip && \
  pip install --no-cache-dir numpy Pillow && \
  apt-get clean && \
  rm -rf "/var/lib/apt/lists"

COPY --from=base /app/bazarr /app/bazarr
RUN mkdir -p /config
EXPOSE 6767
VOLUME ["/config"]
ENV TZ=Europe/Berlin
HEALTHCHECK --start-period=20s --interval=1m --timeout=5s CMD /usr/bin/curl -fsSL http://localhost:6767 || exit 1

ENTRYPOINT [ "python3", "/app/bazarr/bazarr.py", "--no-update", "--config", "/config"]

# Container labels
ARG BUILD_DATE
ARG BUILD_REF
LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-bazarr"
LABEL org.label-schema.name="Bazarr" 
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${BUILD_REF} 
LABEL org.label-schema.name="Bazarr"
LABEL org.label-schema.schema-version="$BAZARR_VERSION"
