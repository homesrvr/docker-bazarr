# docker-bazarr

[![pipeline status](https://gitlab.com/homesrvr/docker-bazarr/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-bazarr/commits/main) 
[![Bazarr Release](https://gitlab.com/homesrvr/docker-bazarr/-/jobs/artifacts/main/raw/release.svg?job=publish_badge)](https://gitlab.com/homesrvr/docker-bazarr/-/jobs/artifacts/main/raw/release.txt?job=publish_badge)
[![Docker link](https://gitlab.com/homesrvr/docker-bazarr/-/jobs/artifacts/main/raw/dockerimage.svg?job=publish_badge)](https://hub.docker.com/r/culater/bazarr)
[![](https://img.shields.io/docker/image-size/culater/bazarr/latest)](https://hub.docker.com/r/culater/bazarr)
[![](https://img.shields.io/docker/pulls/culater/bazarr?color=%23099cec)](https://hub.docker.com/r/culater/bazarr)

This is part of a collection of docker images, designed to run on my low-end x86 based QNAP NAS server.

# Docker image
The resulting docker image can be found here [https://hub.docker.com/r/culater/bazarr](https://hub.docker.com/r/culater/bazarr)

## Example usage

docker cli run example:

```sh
docker run -d \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/Berlin \
  -p 6767:6767 \
  -v /path/to/config:/config \
  --restart unless-stopped \
  culater/bazarr
```

docker compose example:
```
---
version: '3'  
  radarr:
    image: culater/bazarr:latest
    container_name: bazarr
    hostname: bazarr
    environment:
      - PUID=1001
      - PGID=1000
      - TZ=Europe/Berlin
    volumes:
      - /path/to/config:/config
      - /path/to/movies:/movies
      - /path/to/tvshows:/tvshows
    ports:
      - 6767:6767
    restart: always
```

## Reporting problems
Please report any issues to the [Gitlab issue tracker](https://gitlab.com/homesrvr/docker-bazarr/-/issues)

## Authors and acknowledgment
More information about bazarr can be found here:
[Bazarr Project](https://www.bazarr.media/ "Bazarr Project Homepage") 


## Project status
The docker image auto-updates after a new release of bazarr within a few days. The  bazarr version this docker build is based  on can be seen from the release tag. 

